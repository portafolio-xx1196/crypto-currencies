# Use a lightweight base image
FROM golang:1.19.7-alpine3.17 AS build

# Install build dependencies
RUN apk add --no-cache ca-certificates git

# Set environment variables
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

# Set the working directory
WORKDIR /app

# Build the binary
COPY . ./
RUN go build -ldflags="-w -s" -o /crypto-coins

# Use a minimal base image
FROM alpine:3.17
COPY --from=build /crypto-coins /crypto-coins
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# Set the command to run the binary
ENTRYPOINT ["/crypto-coins"]
