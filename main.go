package main

import (
	"CryptoGo/structs"
	"fmt"
	"io"
	"net/http"
)

func main() {
	var input string
	fmt.Print("Ingrese una moneda: ")
	_, err := fmt.Scan(&input)
	if err != nil {
		return
	}
	fmt.Println("La moneda ingresada es:", input)
	fmt.Println("Solicitando la info a CoinGecko")
	price := getPrice(input)
	fmt.Printf("el valor de la moneda %s es de %f", input, price)
}

func getPrice(coin string) float64 {
	var url = fmt.Sprintf("https://api.coingecko.com/api/v3/coins/%s?localization=false&tickers=false&community_data=false&developer_data=false&sparkline=false", coin)

	resp, err := http.Get(url)

	if err != nil {
		return 0
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			panic(err)
		}
	}(resp.Body)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0
	}

	coinStruct, err := structs.UnmarshalCoin(body)

	if err != nil {
		return 0
	}

	return coinStruct.MarketData.CurrentPrice["usd"]
}
