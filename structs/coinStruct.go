// coin.go

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    coin, err := UnmarshalCoin(bytes)
//    bytes, err = coin.Marshal()

package structs

import "encoding/json"

func UnmarshalCoin(data []byte) (Coin, error) {
	var r Coin
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Coin) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Coin struct {
	ID          string      `json:"id"`
	Symbol      string      `json:"symbol"`
	Name        string      `json:"name"`
	Description Description `json:"description"`
	MarketData  MarketData  `json:"market_data"`
}

type Description struct {
	En string `json:"en"`
}

type MarketData struct {
	CurrentPrice map[string]float64 `json:"current_price"`
}
